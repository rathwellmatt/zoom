package com.csc472.zoomtruck.zoom.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<FoodItems> ITEMS = new ArrayList<FoodItems>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, FoodItems> ITEM_MAP = new HashMap<String, FoodItems>();

    /*private static final FoodItems[] FOODITEMS = {
            new FoodItems("Vegetable Pizza",
                    FoodItems.Type.Pizza,
                    "Onions, peppers and jalapeno Pizza",
                    "Vegetable Pizza with grilled onions, fried green peppers, jalapeno, garlic and ginger."),

            new FoodItems("Chicken Burger",
                    FoodItems.Type.Burger,
                    "Chicken Burger",
                    "Chicken Burger with a soft layer of bun in between burger with mayonnaise meshed with it."),

            new FoodItems("Gyro Burritos",
                    FoodItems.Type.Burritos,
                    "Gyro Burritos",
                    "Gyro Burritos with vegetables chopped with meshed sauces."),

            new FoodItems("Chicken Pizza",
                    FoodItems.Type.Pizza,
                    "Add ons with onions, peppers and jalapeno Pizza",
                    "Chicken Pizza with addons - grilled onions, fried green peppers, jalapeno, garlic and ginger."),

            new FoodItems("Gyro Burger",
                    FoodItems.Type.Burger,
                    "Gyro Burger",
                    "Gyro Burger with a soft layer of bun in between burger with mayonnaise meshed with it."),

            new FoodItems("Chicken Burritos",
                    FoodItems.Type.Burritos,
                    "Chicken Burritos",
                    "Chicken Burritos with vegetables chopped with meshed sauces."),

            new FoodItems("Meat Lovers Pizza",
                    FoodItems.Type.Pizza,
                    "Onions, peppers and jalapeno Pizza",
                    "Homemade thin crust pizza, topped off with two types of cheese, bacon, ham, pepperoni and hot sausage! A must make for meat lover’s. "),

            new FoodItems("Hot and Sour Soup",
                    FoodItems.Type.Soup,
                    "The soup contains ingredients to make it both spicy and sour",
                    "Common key ingredients in the include bamboo shoots, toasted sesame oil, wood ear, cloud ear fungus, day lily buds, vinegar, egg, corn starch, and white pepper. Other ingredients include button mushrooms and small slices of tofu skin. This soup is usually considered a healthy option and, other than being high in sodium, is a very healthy soup overall."),

            new FoodItems("Grilled Sheek Kebabs",
                    FoodItems.Type.Kebabs,
                    "",
                    "Seekh Kababs are spicy kababs made from a smooth minced mixture. They can be ordered with either grilled over a bbq or baked in the oven or pan-fried on the stove-top, and are commonly served with a salad, fries, naan or pita bread and a variety of dips."),

    };*/
    static {
        addItem(new FoodItems("Chicken Salad",
                FoodItems.Type.Salad,
                "In a mixing bowl, toss together the chicken, celery, scallions and herbs",
                "In a medium sized bowl, whisk together the mayonnaise, lemon juice, mustard, salt and pepper to taste. Chicken and mixed gently until combined."));
        addItem(new FoodItems("Falafel Panini",
                FoodItems.Type.Panini,
                "Served with garlic souce and Hummus",
                "The bread is cut horizontally and filled with deli ingredients such as cheese and served warm after having been pressed by a warming grill"));
        addItem(new FoodItems("Chicken Nuggets",
                FoodItems.Type.Nuggets,
                "Served with garlic souce and tomato ketchup",
                "Chicken Nugget made from either meat slurry or chicken breasts cut to shape, breaded or battered, then deep-fried or baked. Fried in vegetable oil."));
        addItem(new FoodItems("Garlic Butter Noodles",
                FoodItems.Type.Noodles,
                "Mesmersing buttered Noodles. Makes mouth watery and addictive",
                "Melted butter with oil in a small skillet or saucepan. Poured on the garlic butter and tossed well. Served immediately.\n"));
        addItem(new FoodItems("Sun Dried Tomato Pasta",
                FoodItems.Type.Pasta,
                "Sweet and flavorful sauce with angel hair pasta",
                "Height-of-summer tomatoes burst with flavor and creates little embellised spectacular dish. Churning the oil into boiling liquid emulsifies the mixture, yielding a creamy sauce that coats."));
        /*addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));
        addItem(new FoodItems("3", "Item 3"));*/
    }

    private static void addItem(FoodItems item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.getName(), item);
    }

    /**
     * A dummy item representing a piece of content.

    public static class FoodItems {
        public String id;
        public String content;

        public FoodItems(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }*/
}
