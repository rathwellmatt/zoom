package com.csc472.zoomtruck.zoom;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.csc472.zoomtruck.zoom.dummy.DummyContent;
import com.csc472.zoomtruck.zoom.dummy.FoodItems;

import java.util.HashMap;
import java.util.Map;

/**
 * A list fragment representing a list of FoodItems. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link FoodItemDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class FoodItemListFragment extends ListFragment {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(String id) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FoodItemListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO: replace with a real list adapter.

        /*setListAdapter(new ArrayAdapter<FoodItems>(
                getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                DummyContent.ITEMS));*/
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(DummyContent.ITEMS.get(position).getName());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    static class FoodItemsAdpater extends BaseAdapter {

        private LayoutInflater inflater;
        private Map<FoodItems.Type, Bitmap> icons;

        FoodItemsAdpater(Context context) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            icons = new HashMap<FoodItems.Type, Bitmap>();
            icons.put(FoodItems.Type.Pizza, BitmapFactory.decodeResource(context.getResources(), R.drawable.pizza));
            icons.put(FoodItems.Type.Burger, BitmapFactory.decodeResource(context.getResources(), R.drawable.burger));
            icons.put(FoodItems.Type.Nuggets, BitmapFactory.decodeResource(context.getResources(), R.drawable.nuggets));
            icons.put(FoodItems.Type.Panini, BitmapFactory.decodeResource(context.getResources(), R.drawable.panini));
        }

        @Override
        public int getCount() {
            return DummyContent.ITEMS.size();
        }

        @Override
        public Object getItem(int i) {
            return DummyContent.ITEMS.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.activity_fooditem_list, parent, false);
                holder = new ViewHolder();
                holder.icon = (ImageView) row.findViewById(R.id.imgFoodItem);
                holder.name = (TextView) row.findViewById(R.id.txtLabel);
                holder.description = (TextView) row.findViewById(R.id.txtShortDesc);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            FoodItems item = DummyContent.ITEMS.get(position);
            holder.name.setText(item.getName());
            holder.description.setText(item.getShortDescription());
            holder.icon.setImageBitmap(icons.get(item.getType()));
            return row;
        }

        static class ViewHolder {
            TextView name;
            TextView description;
            ImageView icon;
        }

    }


    /*class FoodItemsAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        @Override
        public int getCount() {
            return DummyContent.ITEMS.size();
        }

        @Override
        public Object getItem(int i) {
            return DummyContent.ITEMS.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (convertView == null) {
                if (inflater == null)
                    inflater = (LayoutInflater) FoodItemListFragment.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.activity_fooditem_list, parent, false);
            }

            ImageView icon = (ImageView) row.findViewById(R.id.imgFoodItem);
            TextView name = (TextView) row.findViewById(R.id.txtLabel);
            TextView description = (TextView) row.findViewById(R.id.txtShortDesc);

            FoodItems fooditem = DummyContent.ITEMS.get(position);
            name.setText(fooditem.getName());
            description.setText(fooditem.getShortDescription());
            icon.setImageResource(FoodItems.getIconResource(fooditem.getType()));
            return row;
        }
    }*/
}
